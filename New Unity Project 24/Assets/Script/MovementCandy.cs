﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCandy : MonoBehaviour {
	CandyManager controller = CandyManager.instance;
	private GameObject tempTiles1;
	CandyController controller1;
	CandyController controller2;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	CandyManager.instance.Matching();
	//CandyManager.instance.NewHorizontal(tempTiles1);
	//CandyManager.instance.PotentialMatchVertical();
		if (Input.GetMouseButtonDown (0)) {
			if (tempTiles1 == null) {
				Select ();
			} else {
				Moving ();
			}
		}
	}
	void Select(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.GetRayIntersection (ray, Mathf.Infinity);
		if (hit) {
			tempTiles1 = hit.transform.gameObject;
			controller1 =  tempTiles1.GetComponent<CandyController> ();
			//Debug.Log(controller1.candyModel.CandyId);
		}

	}
	void Moving(){
		if (tempTiles1 != null) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit2D hit = Physics2D.GetRayIntersection (ray, Mathf.Infinity);
			if (hit) {
				Vector2 tempPos1 = tempTiles1.transform.localPosition;
				Vector2 tempPos2 = hit.transform.localPosition;
				int hClamp = (int )Mathf.Abs(tempPos1.x - tempPos2.x);
				int vClamp = (int )Mathf.Abs(tempPos1.y - tempPos2.y);
				if((hClamp ==1 ) && (vClamp==0)|| (hClamp==0) && (vClamp==1)){
					tempTiles1.transform.localPosition = tempPos2;
					hit.transform.localPosition = tempPos1;
					CandyManager.instance.SwitchCandy(tempTiles1,hit.transform.gameObject);
					//Debug.Log(tempTiles1.transform.position);
					tempTiles1 =null;
					controller2 = hit.transform.gameObject.GetComponent<CandyController> ();
					//Debug.Log(hit.transform.gameObject.GetComponent<Candy>().CandyId);
				}
			}
		}
	}
}
