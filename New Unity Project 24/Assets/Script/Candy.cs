﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Candy{
	//handles x,y
	public int columns;
	public int rows;
	public GameObject o;
	public int CandyId;

	public Candy(){
		CandyId = -1;
	}
	public Candy(int col , int rows , GameObject o , int id){
		this.columns = col;
		this.rows = rows;
		this.o = o;
		this.CandyId = id;
	}

}
