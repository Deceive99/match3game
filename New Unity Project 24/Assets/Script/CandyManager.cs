﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyManager : MonoBehaviour {
	public static CandyManager instance;
	public Sprite[] candySprite;
	public GameObject[] candyGameObject;
	public List<GameObject> listCandy;
	public int columnsBoard = 3;
	public int rowsBoard = 6;
	private Vector2 pos = new Vector2 (-5, 4);
	public Candy[,] tiles;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		//InitCandy ();
		InitGrid ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	void InitCandy(){
		int numbCopy = (columnsBoard * rowsBoard) / 3;
		for (int i = 0; i < numbCopy; i++) {
			for (int x = 0; x < candyGameObject.Length; x++) {
				GameObject o = Instantiate (candyGameObject[x], pos,candyGameObject[x].transform.rotation);
				o.SetActive (false);
				listCandy.Add (o);
			}
		}
	}
	void InitGrid(){
		tiles = new Candy[columnsBoard, rowsBoard];
		for (int x = 0; x < rowsBoard; x++) {
			for (int y = 0; y < columnsBoard; y++) {
				int random = Random.Range (0, candyGameObject.Length);
				GameObject o = (GameObject)Instantiate (candyGameObject [random], new Vector2 (y, x), Quaternion.identity);
				CandyController controller = o.GetComponent<CandyController> ();
				controller.candyManager = this;
				tiles[x,y] = new Candy(x,y,o,random);
				controller.candyModel = tiles[x,y];
			}
		}
	}
	public void SwitchCandy(GameObject candy1, GameObject candy2){
		Candy tempCandy1 = tiles[(int)candy1.transform.position.y,(int)candy1.transform.position.x];
		Candy tempCandy2 = tiles[(int)candy2.transform.position.y,(int)candy2.transform.position.x];
		tiles[(int)candy1.transform.position.y,(int)candy1.transform.position.x] = tempCandy2;
		tiles[(int)candy2.transform.position.y,(int)candy2.transform.position.x] = tempCandy1;		
	}
	public void Check(GameObject candy1){
		Debug.Log(tiles[(int)candy1.transform.position.y,(int)candy1.transform.position.x].o);
	}
	public void Matching(){
		int Count = 1;
		//horizontal
		for(int x = 0 ; x < rowsBoard ; x ++){
			for(int y = 1 ; y < columnsBoard ; y++){
				if(tiles[x,y] != null && tiles[x,y - 1] != null){
					if(tiles[x,y].CandyId == tiles[x,y - 1].CandyId){
						Count ++;
					}else{
						Count = 1;
					}
					if(Count == 3){
						if(tiles[x,y] != null){
							tiles[x,y].o.SetActive(false);
							tiles[x,y] = null;
						}
						if(tiles[x,y-1] != null){
							tiles[x,y-1].o.SetActive(false);
							tiles[x,y-1] = null;
						}
						if(tiles[x,y-2] !=null){
							tiles[x,y-2].o.SetActive(false);
							tiles[x,y-2] = null;
						}
						Count = 1;
					}
				}
			}
		}
		//vertical
		for(int x = 0 ; x < rowsBoard ; x ++){
			for(int y = 1 ; y < columnsBoard ; y++){
				if(tiles[y,x] != null && tiles[y-1,x] != null){
					if(tiles[y,x].CandyId == tiles[y-1,x].CandyId){
						Count ++;
					}else{
						Count = 1;
					}
					if(Count == 3){
						if(tiles[y,x] != null){
							tiles[y,x].o.SetActive(false);
							tiles[y,x] = null;
						}
						if(tiles[y-1,x] != null){
							tiles[y-1,x].o.SetActive(false);
							tiles[y-1,x] = null;
						}
						if(tiles[y-2,x] !=null){
							tiles[y-2,x].o.SetActive(false);
							tiles[y-2,x] = null;
						}
						Count = 1;
					}
				}
			}
		}
	}
}
